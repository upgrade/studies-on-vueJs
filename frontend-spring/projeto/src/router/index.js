import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Usuarios from '@/components/Usuarios' 
import AlterarUsuario from '@/components/AlterarUsuario'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/usuarios',
      name: 'Usuarios',
      component: Usuarios
    },
    {
      path:'/alterarUsuario/:id',
      name: 'Alterar Usuario',
      component: AlterarUsuario
      
    }
  ]
})
